/******************************************************************************
 * File:         Queue.c
 * Version:      1.1
 * Date:         2016-03-31
 * Author:       M. van der Sluys, J. Onokiewicz
 * Description:  OPS excercise 5: Queues
 ******************************************************************************/


#include "Queue.h"
#include <stdio.h>
#include <stdlib.h>

void createQueue(queue_t* queue, data_t data) {
  if(queue->lastNode != NULL) {
    deleteQueue(queue);
  }
  node_t* newNode = (node_t*) malloc(sizeof(node_t));
  queue->lastNode = newNode;
  if(newNode != NULL) {
    newNode->data = data;
    newNode->nextNode = newNode;
  }
}

int emptyQueue(const queue_t* queue) {
  return (queue->lastNode == NULL);
}


size_t sizeQueue(const queue_t* queue) {
  size_t size = 1;
  node_t* next = NULL;
  
  if(queue->lastNode == NULL) {
    return 0;
  }

  next = queue->lastNode->nextNode;
  while(next != queue->lastNode) {
    size++;
    next = next->nextNode;
  }
  return size;
}


data_t* frontQueue(const queue_t* queue) {
  return &(queue->lastNode->nextNode->data);
}


data_t* backQueue(const queue_t* queue) {
  return &(queue->lastNode->data);
}


void pushQueue(queue_t* queue, data_t data) {
  if (queue->lastNode == NULL) {
    createQueue(queue, data);
    return;
  }
  node_t* newNode = (node_t*) malloc(sizeof(node_t));
  if(newNode != NULL) {
    newNode->data = data;                               // Set data
    newNode->nextNode = queue->lastNode->nextNode;  // New last node points to first node
    queue->lastNode->nextNode = newNode;             // Old last node points to new last node
    queue->lastNode = newNode;                        // Set new last-node pointer
  }
}


void popQueue(queue_t* queue) {
  if(queue->lastNode != NULL) {
    node_t* pDelete = queue->lastNode->nextNode;
    if(pDelete == queue->lastNode) {  // Case n=1: make queue empty:
      queue->lastNode = NULL;
    } else {                          // Case n>1: point lastNode to next first node:
      queue->lastNode->nextNode = pDelete->nextNode;
    }
    free(pDelete);
  }
}



void deleteQueue(queue_t* queue) {

  if(queue->lastNode != NULL){
    node_t* pDelete = queue->lastNode->nextNode;
    node_t* pNext = pDelete->nextNode;

    while(pDelete != queue->lastNode)
      {
	free(pDelete);
	pDelete = pNext;
	pNext = pDelete->nextNode;
      }
    free(pDelete);
    queue->lastNode = NULL;

  }
}




void showQueue(const queue_t* queue) {
  const node_t* nextN = queue->lastNode;
  if(nextN == NULL) {
    printf("Queue is empty, last node is %p\n", (void*) queue->lastNode);
  } else {
    printf("Queue contains %ld nodes:\n", sizeQueue(queue));  // Add number
    printf("Last node: %p\n", (void*) queue->lastNode);
    do {
      nextN = nextN->nextNode;
      printf("pNode = %p  Data = '%d' '%s'  nextN = %p\n",
             (void*)nextN, nextN->data.intVal, nextN->data.text, (void*)nextN->nextNode);
    } while(nextN != queue->lastNode);
  }
}
