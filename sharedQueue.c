/******************************************************************************
 * File:         main.c
 * Version:      >9000
 * Date:         the future
 * Author:       Your boys, lil. Dinant & MC Stephan
 * Description:  OPS excercise 9: DANGER!!! Threads and QHUEHUEHUEHUEHUE's
 ******************************************************************************/


#include "Queue.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include <string.h>
#include <signal.h>

typedef struct {
  int sleepSec;
  data_t pData;
} arg_t;

void *producer(void *Parg);
void *consumer();
void handle_signal();

pthread_mutex_t myMutex = PTHREAD_MUTEX_INITIALIZER;

queue_t queue = {NULL};  // Note: element of queue = NULL

bool canRun = true;

int main(void) { 
  
  arg_t arg1 = {2,{1,"Hello qhuehue"}};
  arg_t arg2 = {3,{2,"Hello qhuehuehue"}};
  arg_t arg3 = {4,{3,"Hello qhuehuehuehue"}}; 
  
  pthread_t threadID[4];

  struct sigaction act;
  memset(&act, '\0', sizeof(act));
  act.sa_handler = handle_signal;
  act.sa_flags = 0;
  sigemptyset(&act.sa_mask);  
  
  sigaction(SIGINT, &act, NULL);
    
  pthread_create(&threadID[0], NULL, producer, (void*) &arg1);
  pthread_create(&threadID[1], NULL, producer, (void*) &arg2);
  pthread_create(&threadID[2], NULL, producer, (void*) &arg3);

  pthread_create(&threadID[3], NULL, consumer,NULL);      
      
  for(int i=0; i<4; i++)
    {
      pthread_join(threadID[i], NULL);
    }    

  deleteQueue(&queue);
 
  return 0;
}

void *producer(void *Parg)
{
  arg_t* argP = (arg_t*)Parg;

  while(canRun)
    {
      sleep(argP->sleepSec);
      pthread_mutex_lock(&myMutex);
      pushQueue(&queue,argP->pData);
      pthread_mutex_unlock(&myMutex);
      printf("%d\n",argP->sleepSec);//debug      
    }

  pthread_exit(NULL);
}
void *consumer(){
  FILE* Qhuehuefile;
  Qhuehuefile = fopen("Qhuehuefile.txt", "a");

  while(canRun) {

    sleep(15);
    pthread_mutex_lock(&myMutex);
     
    unsigned int size = sizeQueue(&queue);
     
    for(unsigned int i = 0; i < size; i++){
    
      printf("Int: %d, Text: %s\n", frontQueue(&queue)->intVal, frontQueue(&queue)->text);      //prints Val to stdoutt
      //appends these data to a file
      fprintf(Qhuehuefile,"%d, %s\n",frontQueue(&queue)->intVal, frontQueue(&queue)->text);
      popQueue(&queue);                 //and empties the queue
    }
  
    pthread_mutex_unlock(&myMutex);     //  Unlock/release  mutex 
  }

  fclose(Qhuehuefile);

  pthread_exit(NULL);
}

void handle_signal()
{
  canRun = false;
}
